# 概要

GCPUG-Shonan-vol43 向けに、GKEの設定を保存

## 資料

やりたいことは、簡単にこちらの資料に記載しました。

<https://docs.google.com/document/d/1aoO90w_HCEgGCygdWVS5ytRudOxtVtom5Lz_8ge7qyY/edit#>

## 構築コマンド

ほぼ、以下のチュートリアルの内容です。  
<https://cloud.google.com/kubernetes-engine/docs/tutorials/persistent-disk/>

※英語と日本語ドキュメントで全然違う気がします。英語を見たほうがいいかな。

これに、 ingress の設定を追加してみました。

### クラスタ作成

#### gcloud の設定

project,zoneをセット

``` bash
gcloud config set project [PROJECT_ID]
gcloud config set compute/zone [COMPUTE_ENGINE_ZONE]
```

#### cluster作成コマンド

default設定

``` bash
gcloud container clusters create kube-wordpress --num-nodes=3
```

節約モード (node: 1, preemptible)

``` bash
gcloud container clusters create kube-wordpress  --num-nodes=1  --preemptible
```

※ 参考

既存のcluster を操作する際の資格取得コマンド

``` bash
gcloud container clusters get-credentials kube-wordpress
```

#### volumeclaim作成

disk作成

``` bash
kubectl apply -f mysql-volumeclaim.yaml
kubectl apply -f wordpress-volumeclaim.yaml
```

PersistentVolumeClaims の取得

``` bash
kubectl get pvc
```

#### MySQL作成

secret作成

``` bash
kubectl create secret generic mysql --from-literal=password=YOUR_PASSWORD
```

mysql pod 作成

``` bash
kubectl create -f mysql.yaml
```

mysql の pod情報取得

``` bash
kubectl get pod -l app=mysql
```

#### MySQL service 作成

mysqlサービス作成

``` bash
kubectl create -f mysql-service.yaml
```

mysql service 情報取得

``` bash
kubectl get service mysql
```

#### WordPress 設定

WordPress pod 作成

``` bash
kubectl create -f wordpress.yaml
```

WordPress 情報取得

``` bash
kubectl get pod -l app=wordpress
```

#### WordPress Service 作成

WordPress Serive 作成

``` bash
kubectl create -f wordpress-service.yaml
```

WordPress Serive 情報取得

``` bash
kubectl get svc -l app=wordpress
```

#### wordpress更新

WordPressの更新をした場合

``` bash
kubectl apply -f wordpress.yaml
```

#### ingress

``` bash
kubectl apply -f basic-ingress.yaml
```

#### delete

gcloud container clusters delete kube-wordpress
